export interface ISkill {
  title: string
  experience: string
}

export const skills: ISkill[] = [
  {
    title: 'HTML',
    experience: '92%',
  },
  {
    title: 'CSS',
    experience: '94%',
  },
  {
    title: 'Java Script',
    experience: '87%',
  },
  {
    title: 'Vue',
    experience: '89%',
  },
  {
    title: 'React',
    experience: '43%',
  },
]
