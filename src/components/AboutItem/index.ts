export interface IAbout {
  title: string
  description: string
}

export const about: IAbout[] = [
  {
    title: 'Birthday',
    description: '1 January 1995',
  },
  {
    title: 'Age',
    description: '27',
  },
  {
    title: 'Email',
    description: 'jon199995@gmail.com',
  },
  {
    title: 'Phone',
    description: '+79112103991',
  },
  {
    title: 'City',
    description: 'St. Petersburg',
  },
  {
    title: 'Telegram',
    description: '@EvgeniiZhukovv',
  },
]
