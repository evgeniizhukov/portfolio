export interface IContact {
  link: string
  description: string
  logo: string[]
}

export const contacts: IContact[] = [
  {
    link: 'tg://resolve?domain=EvgeniiZhukovv',
    description: '@EvgeniiZhukovv',
    logo: ['fas', 'paper-plane'],
  },
  {
    link: 'mailto:jon199995@gmail.com',
    description: 'jon199995@gmail.com',
    logo: ['fas', 'envelope-open-text'],
  },
  {
    link: 'tel:+74951234567',
    description: '+7 911 210 39 91',
    logo: ['fas', 'phone-flip'],
  },
  {
    link: 'https://gitlab.com/evgeniizhukov/portfolio',
    description: 'GitLab',
    logo: ['fas', 'code-branch'],
  },
]
