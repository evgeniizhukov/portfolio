import { IResourceBase } from '../base.model'

export interface ICharacterLocation {
  name: string
  url: string
}

export interface ICharacter extends IResourceBase {
  status: CharacterStatus
  species: string
  type: string
  origin: ICharacterLocation
  location: ICharacterLocation
  gender: CharacterGender
  image: string
  episode: string[]
}

export interface ICharacterFilter {
  name?: string
  type?: string
  species?: string
  status?: CharacterStatus
  gender?: CharacterGender
  page?: number
}

export enum CharacterStatus {
  DEAD = 'Dead',
  ALIVE = 'Alive',
  UNKNOWN = 'unknown',
}

export enum CharacterGender {
  FEMALE = 'Female',
  MALE = 'Male',
  GENDERLESS = 'Genderless',
  UNKNOWN = 'unknown',
}
