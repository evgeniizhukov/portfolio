import { ILink } from '@/Services/link.model'

interface ISideBarItem extends ILink {
  logo: string[]
}

export const itemLinks: ISideBarItem[] = [
  {
    name: 'Home',
    path: '/',
    logo: ['fas', 'house-laptop'],
  },
  {
    name: 'About',
    path: '/About',
    logo: ['fas', 'user-large'],
  },
  {
    name: 'Experience',
    path: '/ExperienceAndEducation',
    logo: ['fas', 'screwdriver-wrench'],
  },
  {
    name: 'Rick and Morty',
    path: '/RickAndMorty',
    logo: ['fas', 'beer-mug-empty'],
  },
  {
    name: 'Contacts',
    path: '/Contacts',
    logo: ['fas', 'comments'],
  },
]
