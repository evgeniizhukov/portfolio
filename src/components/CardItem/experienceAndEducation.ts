export interface IExperienceAndEducation {
  title: string
  subTitle: string
  description: string
}

export const experience: IExperienceAndEducation[] = [
  {
    title: '2021 - ...',
    subTitle: 'ThriveDX SaaS (Formerly Cybint Solutions)',
    description:
      'Participation in the development and support of the software product.',
  },
  {
    title: '2021 - 2021',
    subTitle: 'League of Digital Economy',
    description:
      'Participation in the development and support of the software product.',
  },
]

export const education: IExperienceAndEducation[] = [
  {
    title: '2021',
    subTitle: 'Udemy',
    description:
      'Detailed website development training: working with graphic editors (Photoshop, Zeplin, Figma, Avocode...), basics of JS and Jquery, working with Bootstrap 4/5 and FlexBox technology, CSS Grid, using a preprocessor, BEM methodology...',
  },
  {
    title: '2020 - 2021',
    subTitle: 'National Research University ITMO',
    description:
      'System analysis, design, application programming and local / network software development using the latest technologies.',
  },
  {
    title: '2013 - 2017',
    subTitle: 'St. Petersburg Electrotechnical University "LETI"',
    description:
      'Graduated from the Faculty of Electrical Engineering and Automation (FEA). Special training integrates knowledge of control theory, information technology, professional programming and aims to prepare graduates to participate in the design, creation, configuration of local and distributed control systems.',
  },
]
