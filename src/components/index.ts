import UiBurger from './Burger/UiBurger.vue'
import UiButton from './Button/UiButton.vue'
import NavLink from './SidebarPlugin/NavLink.vue'
import { itemLinks } from './SidebarPlugin/index'
import UserTheme from './Theme/UserTheme.vue'
import AboutItem from './AboutItem/AboutItem.vue'
import { about as aboutList } from './AboutItem/index'
import SkillItem from './SkillItem/SkillItem.vue'
import { skills as skillList } from './SkillItem/index'
import CardItem from './CardItem/CardItem.vue'
import { contacts as contactsList } from './contactItem/index'
import ContactItem from './ContactItem/ContactItem.vue'
import PreLoader from './PreLoader/PreLoader.vue'
import {
  education as educationList,
  experience as experienceList,
} from './CardItem/ExperienceAndEducation'

const components = {
  UiBurger,
  UiButton,
  NavLink,
  UserTheme,
  AboutItem,
  SkillItem,
  CardItem,
  ContactItem,
  aboutList,
  skillList,
  educationList,
  experienceList,
  contactsList,
  PreLoader,
}

export {
  UiBurger,
  UiButton,
  NavLink,
  UserTheme,
  AboutItem,
  ContactItem,
  CardItem,
  SkillItem,
  skillList,
  aboutList,
  itemLinks,
  educationList,
  experienceList,
  contactsList,
  PreLoader,
}

export default components
