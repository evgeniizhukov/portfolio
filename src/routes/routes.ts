// Layouts
import LayoutPanel from '../layout/LayoutPanel.vue'

// Pages
import Promo from '../pages/Home.vue'
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

/**
 * Routes
 */
const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: LayoutPanel,
    children: [
      {
        path: '',
        name: 'Home',
        component: Promo,
      },
      {
        path: '/About',
        name: 'About',
        component: () => import('@/pages/About.vue'),
      },
      {
        path: '/ExperienceAndEducation',
        name: 'ExperienceAndEducation',
        component: () => import('@/pages/ExperienceAndEducation.vue'),
      },
      {
        path: '/RickAndMorty',
        name: 'RickAndMorty',
        component: () => import('@/pages/RickAndMorty.vue'),
      },
      {
        path: '/Contacts',
        name: 'Contacts',
        component: () => import('@/pages/Contacts.vue'),
      },
    ],
  },
]

export const router = createRouter({
  history: createWebHashHistory(),
  routes: routes,
})
