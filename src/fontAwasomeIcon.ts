import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faBeerMugEmpty,
  faMoon,
  faSun,
  faHouseLaptop,
  faUserLarge,
  faScrewdriverWrench,
  faBriefcase,
  faComments,
  faCalendar,
  faPaperPlane,
  faPhoneFlip,
  faEnvelopeOpenText,
  faCodeBranch,
  faCircleChevronRight,
  faCircleChevronLeft,
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faBeerMugEmpty,
  faCodeBranch,
  faMoon,
  faSun,
  faHouseLaptop,
  faUserLarge,
  faScrewdriverWrench,
  faBriefcase,
  faComments,
  faCalendar,
  faPaperPlane,
  faPhoneFlip,
  faEnvelopeOpenText,
  faCircleChevronRight,
  faCircleChevronLeft
)

export default FontAwesomeIcon
