import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { router } from './routes/routes'
import FontAwesomeIcon from './fontAwasomeIcon'
import App from './App.vue'
import http from './API/http'
import { AxiosKey } from './API/symbols'

createApp(App)
  .use(createPinia())
  .provide(AxiosKey, http)
  .use(router)
  .component('font-awesome-icon', FontAwesomeIcon)
  .mount('#app')
