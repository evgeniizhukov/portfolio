import {
  CharacterGender,
  CharacterStatus,
  ICharacter,
} from '@/components/rickAndMorty/Services/characters/character.model'
import { defineStore } from 'pinia'
import { IInfo, ILoader } from './home.type'

const info = {
  count: 0,
  next: null,
  pages: 0,
  prev: null,
}

export const loaderState = defineStore({
  id: 'loading',
  state: (): ILoader => ({
    loading: false,
  }),

  getters: {
    getLoadingState(state): boolean {
      return state.loading
    },
  },

  actions: {
    setLoadingState(loadingState: boolean): void {
      this.$state.loading = loadingState
    },
  },
})

export const charactersState = defineStore({
  id: 'characters',
  state: (): IInfo<ICharacter[]> => ({
    info,
    results: [
      {
        id: -1,
        name: '',
        url: '',
        created: '',
        status: CharacterStatus.UNKNOWN,
        species: '',
        type: '',
        origin: {
          name: '',
          url: '',
        },
        location: {
          name: '',
          url: '',
        },
        gender: CharacterGender.UNKNOWN,
        image: '',
        episode: [],
      },
    ],
  }),

  getters: {
    getCharactersPage(state): IInfo<ICharacter[]> {
      return state
    },

    getCharacters(state): (start: number, count: number) => ICharacter[] {
      return (start, count) => {
        const startElement = start * count
        const endElement = start * count + count

        return state.results.slice(startElement, endElement)
      }
    },
  },

  actions: {
    setCharacters(charactersState: IInfo<ICharacter[]>): void {
      this.$state = charactersState
    },
  },
})
