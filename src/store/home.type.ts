export interface IInfo<T> extends IInfoPage {
  results: T
}

export interface IInfoPage {
  info: {
    count: number
    next: string | null
    pages: number
    prev: string | null
  }
}

export interface ILoader {
  loading: boolean
}
