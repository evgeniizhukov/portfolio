# UI for Portfolio by Evgenii Zhukov

## Prerequisites

In order to run this service you will need the following:

- Npm

## Building & Running

1. Install dependencies: `npm install`
2. Serve with hot reload at localhost:8000: `npm run dev`
3. Build for production: `npm run build`
